import os
from setuptools import setup, find_packages

# VERSION = (1, 4, 16)

# Dynamically calculate the version based on VERSION tuple
# if len(VERSION) > 2 and VERSION[2] is not None:
#     str_version = "%d.%d_%s" % VERSION[:3]
# else:
#     str_version = "%d.%d" % VERSION[:2]

# version = str_version

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name = 'django-livesettings',
    version = "1.4.16",
    description = "livesettings",
    long_description = """Django-Livesettings is a project split from the Satchmo Project. It provides the ability to configure settings via an admin interface, rather than by editing "settings.py".""",
    author = 'Nicola Costantino',
    author_email = 'n.costantino@frankhood.com',
    url = 'https://gitlab.com/N.Costantino/django-livesettings',
    license = 'New BSD License',
    platforms = ['any'],
    classifiers = ['Development Status :: 4 - Beta',
                   'Environment :: Web Environment',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: BSD License',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Framework :: Django'],
    packages = find_packages(),
    # setup_requires=["setuptools_hg"],
    include_package_data = True,
)
